package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Activity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "patientId", nullable = false)
    private String patientIde;

    @Column(name = "activity", nullable = false)
    private String activity;

    @Column(name = "astart", nullable = false)
    private String astart;

    @Column(name = "aend", nullable = false)
    private String aend;

    public Activity() {
    }

    public Activity(String patientIde, String activity, String astart, String aend) {
        this.patientIde = patientIde;
        this.activity = activity;
        this.astart = astart;
        this.aend = aend;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPatientIde() {
        return patientIde;
    }

    public void setPatientIde(String patientIde) {
        this.patientIde = patientIde;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getAstart() {
        return astart;
    }

    public void setAstart(String astart) {
        this.astart = astart;
    }

    public String getAend() {
        return aend;
    }

    public void setAend(String aend) {
        this.aend = aend;
    }
}
