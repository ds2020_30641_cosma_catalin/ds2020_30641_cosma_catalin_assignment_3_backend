package ro.tuc.ds2020;

import com.google.gson.Gson;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.repositories.ActivityRepository;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Service
public class RabbitMQListener {
    @Autowired
    ConnectionFactory connectionFactory;

    @Autowired
    Queue queue;

    @Autowired
    SequentialMessageListener sequentialMessageListener;



    @PostConstruct
    public void init() {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setQueues(queue);
        container.setMessageListener(sequentialMessageListener);
        container.start();

    }
}

@Service
class SequentialMessageListener implements MessageListener {

    @Autowired
    private SimpMessagingTemplate webSocket;

    @Autowired
    private final ActivityRepository activityRepository;

    SequentialMessageListener(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @Override
    public void onMessage(Message message) {
        System.out.println("Consuming Message - " + new String(message.getBody()));
        Gson g = new Gson();
        String jsonString = new String(message.getBody());
        SensorData sensorData = g.fromJson(jsonString, SensorData.class);

        Long startDate = Long.parseLong(sensorData.getStart());
        Long endDate = Long.parseLong(sensorData.getEnd());

        Long duration = endDate - startDate;

        Long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        Long hours = TimeUnit.MILLISECONDS.toHours(duration);

        Activity activity = new Activity(sensorData.getPatientId(), sensorData.getActivity(), sensorData.getStart(), sensorData.getEnd());


        if(sensorData.getActivity().equals("Sleeping") && hours > 7){
            System.out.println("Issue detected, sleeping > 7 hours");

           this.webSocket.convertAndSend("/message",  "Issue detected, sleeping > 7 hours");
           activityRepository.save(activity);
        }

        if(sensorData.getActivity().equals("Leaving") && hours > 5){
            System.out.println("Issue detected, Leaving > 5 hours");
            this.webSocket.convertAndSend("/message",  "Issue detected, Leaving > 5 hours");
            activityRepository.save(activity);
        }

        if(sensorData.getActivity().equals("Toileting") && minutes > 30){
            System.out.println("Issue detected, Toileting > 30 minutes");
            this.webSocket.convertAndSend("/message",  "Issue detected, Toileting > 30 minutes");
            activityRepository.save(activity);
        }

    }
}
