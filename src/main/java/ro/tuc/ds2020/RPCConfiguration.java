package ro.tuc.ds2020;

import com.googlecode.jsonrpc4j.spring.JsonServiceExporter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RPCConfiguration {

    @Bean
    public MyService myService() {
        return new MyServiceImpl(null);
    }

    @Bean(name = "/rpc/myservice")
    public JsonServiceExporter jsonServiceExporter() {
        JsonServiceExporter exporter = new JsonServiceExporter();
        exporter.setService(myService());
        exporter.setServiceInterface(MyService.class);
        return exporter;
    }


}
