package ro.tuc.ds2020;

public class SensorData {

    private String patientId;
    private String activity;
    private String start;
    private String end;

    public SensorData(String patientId, String activity, String start, String end) {
        this.patientId = patientId;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "SensorData{" +
                "patientId='" + patientId + '\'' +
                ", activity='" + activity + '\'' +
                ", start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
