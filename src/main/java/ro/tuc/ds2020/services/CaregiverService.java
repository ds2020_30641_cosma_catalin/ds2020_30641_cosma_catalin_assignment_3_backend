package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.repositories.CaregiverRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;


    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }


    public List<Caregiver> findAllCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList;
    }

    public Caregiver findCaregiverById(UUID id) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(id);
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return caregiverOptional.get();
    }

    public UUID insert(Caregiver caregiver) {
        Caregiver savedCaregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public Caregiver update(Caregiver caregiver) {
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(caregiver.getId());
        if (!caregiverOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiver.getId());
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiver.getId());
        }
        Caregiver foundCaregiver = caregiverOptional.get();

        foundCaregiver.setName(caregiver.getName());
        foundCaregiver.setPassword(caregiver.getPassword());
        foundCaregiver.setBirthDate(caregiver.getBirthDate());
        foundCaregiver.setGender(caregiver.getGender());
        foundCaregiver.setAddress(caregiver.getAddress());
        foundCaregiver.setPatients(caregiver.getPatients());

        Caregiver updatedCaregiver = caregiverRepository.save(foundCaregiver);
        LOGGER.debug("Caregiver with id {} was updated in db", caregiver.getId());
        return updatedCaregiver;
    }

    public void delete(UUID id) {
        caregiverRepository.deleteById(id);
        LOGGER.debug("Caregiver with id {} was deleted from the db", id);
    }
}
