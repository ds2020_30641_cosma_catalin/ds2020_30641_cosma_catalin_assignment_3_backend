package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public List<Patient> findAllPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList;
    }

    public Patient findPatientById(UUID id) {
        Optional<Patient> patientOptional = patientRepository.findById(id);
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return patientOptional.get();
    }

    public UUID insert(Patient patient) {
        Patient savedPatient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return savedPatient.getId();
    }

    public Patient update(Patient patient) {
        Optional<Patient> patientOptional = patientRepository.findById(patient.getId());
        if (!patientOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patient.getId());
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patient.getId());
        }
        Patient foundPatient = patientOptional.get();

        foundPatient.setName(patient.getName());
        foundPatient.setPassword(patient.getPassword());
        foundPatient.setAddress(patient.getAddress());
        foundPatient.setBirthDate(patient.getBirthDate());
        foundPatient.setGender(patient.getGender());
        foundPatient.setMedicalRecord(patient.getMedicalRecord());
        foundPatient.setMedicationPlan(patient.getMedicationPlan());

        Patient updatedPatient = patientRepository.save(foundPatient);

        LOGGER.debug("Patient with id {} was updated in db", patient.getId());
        return updatedPatient;
    }

    public void delete(UUID id) {
        patientRepository.deleteById(id);
        LOGGER.debug("Patient with id {} was deleted from the db", id);
    }

}
