package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public List<Medication> findAllMedications() {
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList;
    }

    public Medication findMedicationById(UUID id) {
        Optional<Medication> medicationOptional = medicationRepository.findById(id);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        return medicationOptional.get();
    }

    public UUID insert(Medication medication) {
        Medication savedMedication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return savedMedication.getId();
    }

    public Medication update(Medication medication) {
        Optional<Medication> medicationOptional = medicationRepository.findById(medication.getId());
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", medication.getId());
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + medication.getId());
        }
        Medication foundMedication = medicationOptional.get();

        foundMedication.setName(medication.getName());
        foundMedication.setSideEffects(medication.getSideEffects());
        foundMedication.setDosage(medication.getDosage());

        Medication updatedMedication = medicationRepository.save(foundMedication);
        LOGGER.debug("Medication with id {} was updated in db", medication.getId());
        return updatedMedication;
    }

    public void delete(UUID id) {
        medicationRepository.deleteById(id);
        LOGGER.debug("Medication with id {} was deleted from the db", id);
    }

}
