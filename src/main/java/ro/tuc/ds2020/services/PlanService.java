package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.entities.Plan;
import ro.tuc.ds2020.repositories.PlanRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlanService.class);
    private final PlanRepository planRepository;

    @Autowired
    public PlanService(PlanRepository planRepository) {
        this.planRepository = planRepository;
    }

    public List<Plan> findAllPlans() {
        List<Plan> planList = planRepository.findAll();
        return planList;
    }

    public Plan findPlanById(UUID id) {
        Optional<Plan> planOptional = planRepository.findById(id);
        if (!planOptional.isPresent()) {
            LOGGER.error("Plan with id {} was not found in db", id);
            throw new ResourceNotFoundException(Plan.class.getSimpleName() + " with id: " + id);
        }
        return planOptional.get();
    }

    public UUID insert(Plan plan) {
        Plan savedPlan = planRepository.save(plan);
        LOGGER.debug("Plan with id {} was inserted in db", plan.getId());
        return savedPlan.getId();
    }

    public Plan update(Plan plan) {
        Optional<Plan> planOptional = planRepository.findById(plan.getId());
        if (!planOptional.isPresent()) {
            LOGGER.error("Plan with id {} was not found in db", plan.getId());
            throw new ResourceNotFoundException(Plan.class.getSimpleName() + " with id: " + plan.getId());
        }
        Plan foundPlan = planOptional.get();

        foundPlan.setName(plan.getName());
        foundPlan.setIntervals(plan.getIntervals());
        foundPlan.setDuration(plan.getDuration());
        foundPlan.setMedications(plan.getMedications());

        Plan updatedPlan = planRepository.save(foundPlan);
        LOGGER.debug("Plan with id {} was updated in db", plan.getId());
        return updatedPlan;
    }

    public void delete(UUID id) {
        planRepository.deleteById(id);
        LOGGER.debug("Plan with id {} was deleted from the db", id);
    }
}
