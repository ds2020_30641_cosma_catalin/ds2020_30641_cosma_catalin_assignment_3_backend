package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.repositories.ActivityRepository;

import java.util.List;
import java.util.UUID;

@Service
public class ActivityService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityService.class);
    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public UUID insert(Activity activity) {
        Activity savedActivity = activityRepository.save(activity);
        LOGGER.debug("Activity with id {} was inserted in db", activity.getId());
        return savedActivity.getId();
    }

    public List<Activity> findAllActivities() {
        List<Activity> activitiesList = activityRepository.findAll();
        return activitiesList;
    }

    public void deleteAll() {
        activityRepository.deleteAll();
        LOGGER.debug("All activities were deleted");
    }
}
