package ro.tuc.ds2020;

import org.springframework.http.ResponseEntity;
import ro.tuc.ds2020.entities.Plan;

public interface MyService {

    ResponseEntity<Plan> downloadMedicationPlan(String patientName);

    ResponseEntity<String> displayMedicationStatus(String patientName, String medicationName, String medicationStatus, String statusTime);
}