package ro.tuc.ds2020;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Plan;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;

public class MyServiceImpl implements MyService {

    @Autowired
    private final PatientRepository patientRepository;

    public MyServiceImpl( PatientRepository patientRepository){
        this.patientRepository = patientRepository;
    }

    @Override
    public ResponseEntity<Plan> downloadMedicationPlan(String patientName) {
        List<Patient> patientList = patientRepository.findAll();
        Plan foundPlan = null;
        for(Patient p : patientList){
            if(patientName.equals(p.getName())){
                foundPlan = p.getMedicationPlan();
            }
        }
        return new ResponseEntity<>(foundPlan, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> displayMedicationStatus(String patientName, String medicationName, String medicationStatus, String statusTime) {

        if(medicationStatus.equals("taken")){
            System.out.println("Patient " + patientName + " took " + medicationName + " at " + statusTime + " !");
        }
        else{
            System.out.println("Patient " + patientName + " didn't take " + medicationName + " within the time interval !");
        }

        return new ResponseEntity<>("Message received", HttpStatus.OK);
    }

}