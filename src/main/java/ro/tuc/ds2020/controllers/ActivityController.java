package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.services.ActivityService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/activity")
public class ActivityController {

    private final ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Activity>> getActivities() {
        List<Activity> activities = activityService.findAllActivities();
        return new ResponseEntity<>(activities, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteAll")
    public ResponseEntity deleteAllActivities() {
        activityService.deleteAll();
        return new ResponseEntity(HttpStatus.OK);

    }


}
