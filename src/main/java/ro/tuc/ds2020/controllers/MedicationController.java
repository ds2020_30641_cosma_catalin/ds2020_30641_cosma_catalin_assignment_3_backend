package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Plan;
import ro.tuc.ds2020.services.MedicationService;
import ro.tuc.ds2020.services.PlanService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;
    private final PlanService planService;

    @Autowired
    public MedicationController(MedicationService medicationService, PlanService planService) {
        this.medicationService = medicationService;
        this.planService = planService;
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Medication>> getMedications() {
        List<Medication> medications = medicationService.findAllMedications();
        return new ResponseEntity<>(medications, HttpStatus.OK);
    }

    @GetMapping(value = "/get{id}")
    public ResponseEntity<Medication> getMedication(@PathVariable("id") UUID medicationId) {
        Medication foundMedication = medicationService.findMedicationById(medicationId);
        return new ResponseEntity<>(foundMedication, HttpStatus.OK);
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody Medication medication) {
        UUID medicationID = medicationService.insert(medication);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Medication> updateMedication(@Valid @RequestBody Medication medication) {
        Medication savedMedication = medicationService.update(medication);
        return new ResponseEntity<>(savedMedication, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete{id}")
    public ResponseEntity deleteMedication(@PathVariable("id") UUID medicationId) {
        List<Plan> plans = planService.findAllPlans();
        List<Medication> medsFromPlan = new ArrayList<>();
        List<Medication> medsFinal = new ArrayList<>();
        Medication med = new Medication();
        boolean updateMeds = false;

        for (Plan e1 : plans) {
            updateMeds = false;
            medsFromPlan = e1.getMedications();
            for (Medication e2 : medsFromPlan) {
                if (e2.getId().equals(medicationId)) {
                    medsFinal = medsFromPlan;
                    updateMeds = true;
                    med = e2;
                }
            }

            if (updateMeds) {
                medsFinal.remove(med);
                e1.setMedications(medsFinal);
                planService.update(e1);
            }
        }

        medicationService.delete(medicationId);

        return new ResponseEntity(HttpStatus.OK);
    }

}
