package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }


    @GetMapping(value = "/all")
    public ResponseEntity<List<Caregiver>> getCaregiversDetails() {
        List<Caregiver> dtos = caregiverService.findAllCaregivers();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping(value = "/get{id}")
    public ResponseEntity<Caregiver> getCaregiver(@PathVariable("id") UUID caregiverId) {
        Caregiver caregiver = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(caregiver, HttpStatus.OK);
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody Caregiver caregiverToInsert) {
        UUID caregiverID = caregiverService.insert(caregiverToInsert);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Caregiver> updateCaregiver(@Valid @RequestBody Caregiver caregiver) {
        Caregiver updatedCaregiver = caregiverService.update(caregiver);
        return new ResponseEntity<>(updatedCaregiver, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete{id}")
    public ResponseEntity deleteCaregiver(@PathVariable("id") UUID caregiverId) {
        caregiverService.delete(caregiverId);
        return new ResponseEntity(HttpStatus.OK);
    }
}
