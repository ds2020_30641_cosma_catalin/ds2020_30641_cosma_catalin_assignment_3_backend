package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;
    private final CaregiverService caregiverService;

    @Autowired
    public PatientController(PatientService patientService, CaregiverService caregiverService) {
        this.patientService = patientService;
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Patient>> getPatients() {
        List<Patient> patients = patientService.findAllPatients();
        return new ResponseEntity<>(patients, HttpStatus.OK);
    }

    @GetMapping(value = "/get{id}")
    public ResponseEntity<Patient> getPatient(@PathVariable("id") UUID patientId) {
        Patient patient = patientService.findPatientById(patientId);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<UUID> insertPatient(@Valid @RequestBody Patient patient) {
        UUID patientID = patientService.insert(patient);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Patient> updatePatient(@Valid @RequestBody Patient patient) {
        Patient updatedPatient = patientService.update(patient);
        return new ResponseEntity<>(updatedPatient, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete{id}")
    public ResponseEntity deletePatient(@PathVariable("id") UUID patientId) {

        List<Caregiver> caregivers = caregiverService.findAllCaregivers();
        Caregiver caregiver = new Caregiver();
        List<Patient> patientsFromCaregiver = new ArrayList<>();
        List<Patient> patientsFinal = new ArrayList<>();
        Patient patient = new Patient();
        boolean updatePatient = false;

        for (Caregiver e1 : caregivers) {
            updatePatient = false;
            patientsFromCaregiver = e1.getPatients();
            caregiver = e1;
            for (Patient e2 : patientsFromCaregiver) {
                if (e2.getId().equals(patientId)) {
                    patientsFinal = patientsFromCaregiver;
                    updatePatient = true;
                    patient = e2;
                }
            }

            if (updatePatient) {
                patientsFinal.remove(patient);
                caregiver.setPatients(patientsFinal);
                caregiverService.update(caregiver);
            }
        }

        patientService.delete(patientId);

        return new ResponseEntity(HttpStatus.OK);
    }

}