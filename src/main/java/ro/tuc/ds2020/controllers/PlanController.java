package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Plan;
import ro.tuc.ds2020.services.PatientService;
import ro.tuc.ds2020.services.PlanService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/plan")
public class PlanController {

    private final PlanService planService;
    private final PatientService patientService;

    @Autowired
    public PlanController(PlanService planService, PatientService patientService) {
        this.planService = planService;
        this.patientService = patientService;
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Plan>> getPlans() {
        List<Plan> plans = planService.findAllPlans();
        return new ResponseEntity<>(plans, HttpStatus.OK);
    }

    @GetMapping(value = "/get{id}")
    public ResponseEntity<Plan> getPlan(@PathVariable("id") UUID planId) {
        Plan plan = planService.findPlanById(planId);
        return new ResponseEntity<>(plan, HttpStatus.OK);
    }

    @PostMapping(value = "/insert")
    public ResponseEntity<UUID> insertPlan(@Valid @RequestBody Plan plan) {
        UUID planID = planService.insert(plan);
        return new ResponseEntity<>(planID, HttpStatus.CREATED);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Plan> updatePlan(@Valid @RequestBody Plan plan) {
        Plan updatedPlan = planService.update(plan);
        return new ResponseEntity<>(updatedPlan, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete{id}")
    public ResponseEntity deletePlan(@PathVariable("id") UUID planId) {

        List<Patient> patients = patientService.findAllPatients();
        Plan planFromPatient = new Plan();
        boolean deletePlan = false;

        for (Patient e1 : patients) {
            deletePlan = false;
            planFromPatient = e1.getMedicationPlan();

            if (planFromPatient != null) {
                if (planFromPatient.getId().equals(planId)) {
                    deletePlan = true;
                }

                if (deletePlan) {
                    e1.setMedicationPlan(null);
                    patientService.update(e1);
                }
            }
        }

        planService.delete(planId);

        return new ResponseEntity(HttpStatus.OK);
    }
}
