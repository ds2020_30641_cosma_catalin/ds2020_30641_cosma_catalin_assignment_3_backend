package ro.tuc.ds2020;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfiguration {

    @Value("${spring.rabbitmq.host}")
    String host;

    @Value("${spring.rabbitmq.username}")
    String username;

    @Value("${spring.rabbitmq.password}")
    private String password;

    @Bean
    public Queue appQueueGeneric() {
        RabbitAdmin admin = new RabbitAdmin(this.connectionFactory());
        Queue queue = new Queue("appGenericQueue");
        admin.declareQueue(queue);
        TopicExchange exchange = new TopicExchange("appExchange");
        admin.declareExchange(exchange);
        admin.declareBinding(BindingBuilder.bind(queue).to(exchange).with("messages.key"));
        return queue;
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("kangaroo-01.rmq.cloudamqp.com");
        connectionFactory.setUsername("haaosfms");
        connectionFactory.setPassword("b5HLALqf1pqsbfKZ-foR4_899eVF35-_");
        connectionFactory.setVirtualHost("haaosfms");

        return connectionFactory;
    }

}